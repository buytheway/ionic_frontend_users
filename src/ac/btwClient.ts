import {
  ApolloClient,
  createHttpLink,
  InMemoryCache,
  makeVar
} from "@apollo/client"
import { setContext } from "@apollo/client/link/context"
import { Storage } from "@capacitor/storage"
import { createUploadLink } from "apollo-upload-client"
import { User } from "../generated/graphql"
import { ACCESSTOKEN } from "../utils/constants"

const endpointURL = process.env.REACT_APP_ENDPOINTURL
export const apolloCache = new InMemoryCache()

const httpLink = createHttpLink({
  uri: endpointURL,
})
const uploadLink = createUploadLink({
  uri: endpointURL,
})

const authLink = setContext(async (_, { headers }) => {
  // get the authentication token from local storage if it exists
  // const token = localStorage.getItem("accessToken")
  const token = await Storage.get({ key: ACCESSTOKEN })

  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token.value ? `Bearer ${token.value}` : "",
    },
  }
})

export const client = new ApolloClient({
  link: authLink.concat(uploadLink),
  cache: apolloCache,
})

export const currentUserVar = makeVar<User | null>(null)
export const currentUserEmailVar = makeVar<string | null>(null)
