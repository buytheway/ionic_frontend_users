import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};

export type AuthInput = {
  username: Scalars['String'];
  email: Scalars['String'];
  password: Scalars['String'];
};

export type EditUserBasicInfoInput = {
  tel: Scalars['String'];
  address: Scalars['String'];
  zip: Scalars['String'];
  city: Scalars['String'];
  country: Scalars['String'];
};

export type EditUserProfileInput = {
  id: Scalars['String'];
  username?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
};

export type GpsCode = {
  __typename?: 'GpsCode';
  lat?: Maybe<Scalars['Float']>;
  lng?: Maybe<Scalars['Float']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  register: UserResponse;
  login: UserResponse;
  deleteUser?: Maybe<User>;
  editUserProfile?: Maybe<User>;
  editUserPassword_simple: User;
  editUserPassword: User;
  editUserEmail?: Maybe<User>;
  editUserBasicInfo?: Maybe<User>;
  createService?: Maybe<ServiceResponse>;
  singleAvatarUpload: User;
};


export type MutationRegisterArgs = {
  input: AuthInput;
};


export type MutationLoginArgs = {
  password: Scalars['String'];
  email: Scalars['String'];
};


export type MutationDeleteUserArgs = {
  userId: Scalars['String'];
};


export type MutationEditUserProfileArgs = {
  input: EditUserProfileInput;
};


export type MutationEditUserPassword_SimpleArgs = {
  newPassword: Scalars['String'];
  userId: Scalars['String'];
};


export type MutationEditUserPasswordArgs = {
  newPassword: Scalars['String'];
  password: Scalars['String'];
  userId: Scalars['String'];
};


export type MutationEditUserEmailArgs = {
  email: Scalars['String'];
};


export type MutationEditUserBasicInfoArgs = {
  input: EditUserBasicInfoInput;
};


export type MutationCreateServiceArgs = {
  shopUnregistered: ShopUnregisteredInput;
};


export type MutationSingleAvatarUploadArgs = {
  file: Scalars['Upload'];
};

export type Query = {
  __typename?: 'Query';
  hello: Scalars['String'];
  currentUser?: Maybe<User>;
  getGpsFromAddress?: Maybe<GpsCode>;
};


export type QueryHelloArgs = {
  me: Scalars['String'];
};


export type QueryGetGpsFromAddressArgs = {
  address: Scalars['String'];
};

export type ServiceResponse = {
  __typename?: 'ServiceResponse';
  id?: Maybe<Scalars['String']>;
  user?: Maybe<User>;
  shopUnregistered?: Maybe<ShopUnregistered>;
};

export type ShopUnregistered = {
  __typename?: 'ShopUnregistered';
  id?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  address: Scalars['String'];
  gps?: Maybe<Array<Scalars['Float']>>;
  image?: Maybe<Scalars['String']>;
};

export type ShopUnregisteredInput = {
  name: Scalars['String'];
  address: Scalars['String'];
};


export type User = {
  __typename?: 'User';
  id: Scalars['ID'];
  roles: Array<Scalars['String']>;
  username: Scalars['String'];
  avatar?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  createdAt: Scalars['String'];
  tier?: Maybe<Scalars['String']>;
  tel?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  zip?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  gps?: Maybe<Array<Scalars['Float']>>;
  activated?: Maybe<Scalars['Boolean']>;
};

export type UserResponse = {
  __typename?: 'UserResponse';
  user?: Maybe<User>;
  token?: Maybe<Scalars['String']>;
};

export type HelloQueryVariables = Exact<{
  me: Scalars['String'];
}>;


export type HelloQuery = (
  { __typename?: 'Query' }
  & Pick<Query, 'hello'>
);

export type UserDetailsFragment = (
  { __typename?: 'User' }
  & Pick<User, 'id' | 'username' | 'email' | 'avatar' | 'roles' | 'tier' | 'createdAt' | 'tel' | 'address' | 'zip' | 'city' | 'country' | 'gps'>
);

export type CurrentUserQueryVariables = Exact<{ [key: string]: never; }>;


export type CurrentUserQuery = (
  { __typename?: 'Query' }
  & { currentUser?: Maybe<(
    { __typename?: 'User' }
    & UserDetailsFragment
  )> }
);

export type LoginMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginMutation = (
  { __typename?: 'Mutation' }
  & { login: (
    { __typename?: 'UserResponse' }
    & Pick<UserResponse, 'token'>
    & { user?: Maybe<(
      { __typename?: 'User' }
      & UserDetailsFragment
    )> }
  ) }
);

export type EditUserEmailMutationVariables = Exact<{
  email: Scalars['String'];
}>;


export type EditUserEmailMutation = (
  { __typename?: 'Mutation' }
  & { editUserEmail?: Maybe<(
    { __typename?: 'User' }
    & UserDetailsFragment
  )> }
);

export type RegisterMutationVariables = Exact<{
  input: AuthInput;
}>;


export type RegisterMutation = (
  { __typename?: 'Mutation' }
  & { register: (
    { __typename?: 'UserResponse' }
    & Pick<UserResponse, 'token'>
    & { user?: Maybe<(
      { __typename?: 'User' }
      & UserDetailsFragment
    )> }
  ) }
);

export type EditUserBasicInfoMutationVariables = Exact<{
  input: EditUserBasicInfoInput;
}>;


export type EditUserBasicInfoMutation = (
  { __typename?: 'Mutation' }
  & { editUserBasicInfo?: Maybe<(
    { __typename?: 'User' }
    & UserDetailsFragment
  )> }
);

export type EditUserPasswordMutationVariables = Exact<{
  userId: Scalars['String'];
  password: Scalars['String'];
  newPassword: Scalars['String'];
}>;


export type EditUserPasswordMutation = (
  { __typename?: 'Mutation' }
  & { editUserPassword: (
    { __typename?: 'User' }
    & UserDetailsFragment
  ) }
);

export type SingleAvatarUploadMutationVariables = Exact<{
  file: Scalars['Upload'];
}>;


export type SingleAvatarUploadMutation = (
  { __typename?: 'Mutation' }
  & { singleAvatarUpload: (
    { __typename?: 'User' }
    & UserDetailsFragment
  ) }
);

export const UserDetailsFragmentDoc = gql`
    fragment UserDetails on User {
  id
  username
  email
  avatar
  roles
  tier
  createdAt
  tel
  address
  zip
  city
  country
  gps
}
    `;
export const HelloDocument = gql`
    query hello($me: String!) {
  hello(me: $me)
}
    `;

/**
 * __useHelloQuery__
 *
 * To run a query within a React component, call `useHelloQuery` and pass it any options that fit your needs.
 * When your component renders, `useHelloQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useHelloQuery({
 *   variables: {
 *      me: // value for 'me'
 *   },
 * });
 */
export function useHelloQuery(baseOptions: Apollo.QueryHookOptions<HelloQuery, HelloQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<HelloQuery, HelloQueryVariables>(HelloDocument, options);
      }
export function useHelloLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<HelloQuery, HelloQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<HelloQuery, HelloQueryVariables>(HelloDocument, options);
        }
export type HelloQueryHookResult = ReturnType<typeof useHelloQuery>;
export type HelloLazyQueryHookResult = ReturnType<typeof useHelloLazyQuery>;
export type HelloQueryResult = Apollo.QueryResult<HelloQuery, HelloQueryVariables>;
export const CurrentUserDocument = gql`
    query currentUser {
  currentUser {
    ...UserDetails
  }
}
    ${UserDetailsFragmentDoc}`;

/**
 * __useCurrentUserQuery__
 *
 * To run a query within a React component, call `useCurrentUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useCurrentUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCurrentUserQuery({
 *   variables: {
 *   },
 * });
 */
export function useCurrentUserQuery(baseOptions?: Apollo.QueryHookOptions<CurrentUserQuery, CurrentUserQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CurrentUserQuery, CurrentUserQueryVariables>(CurrentUserDocument, options);
      }
export function useCurrentUserLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CurrentUserQuery, CurrentUserQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CurrentUserQuery, CurrentUserQueryVariables>(CurrentUserDocument, options);
        }
export type CurrentUserQueryHookResult = ReturnType<typeof useCurrentUserQuery>;
export type CurrentUserLazyQueryHookResult = ReturnType<typeof useCurrentUserLazyQuery>;
export type CurrentUserQueryResult = Apollo.QueryResult<CurrentUserQuery, CurrentUserQueryVariables>;
export const LoginDocument = gql`
    mutation login($email: String!, $password: String!) {
  login(email: $email, password: $password) {
    user {
      ...UserDetails
    }
    token
  }
}
    ${UserDetailsFragmentDoc}`;
export type LoginMutationFn = Apollo.MutationFunction<LoginMutation, LoginMutationVariables>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(baseOptions?: Apollo.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, options);
      }
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = Apollo.MutationResult<LoginMutation>;
export type LoginMutationOptions = Apollo.BaseMutationOptions<LoginMutation, LoginMutationVariables>;
export const EditUserEmailDocument = gql`
    mutation editUserEmail($email: String!) {
  editUserEmail(email: $email) {
    ...UserDetails
  }
}
    ${UserDetailsFragmentDoc}`;
export type EditUserEmailMutationFn = Apollo.MutationFunction<EditUserEmailMutation, EditUserEmailMutationVariables>;

/**
 * __useEditUserEmailMutation__
 *
 * To run a mutation, you first call `useEditUserEmailMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEditUserEmailMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [editUserEmailMutation, { data, loading, error }] = useEditUserEmailMutation({
 *   variables: {
 *      email: // value for 'email'
 *   },
 * });
 */
export function useEditUserEmailMutation(baseOptions?: Apollo.MutationHookOptions<EditUserEmailMutation, EditUserEmailMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<EditUserEmailMutation, EditUserEmailMutationVariables>(EditUserEmailDocument, options);
      }
export type EditUserEmailMutationHookResult = ReturnType<typeof useEditUserEmailMutation>;
export type EditUserEmailMutationResult = Apollo.MutationResult<EditUserEmailMutation>;
export type EditUserEmailMutationOptions = Apollo.BaseMutationOptions<EditUserEmailMutation, EditUserEmailMutationVariables>;
export const RegisterDocument = gql`
    mutation Register($input: AuthInput!) {
  register(input: $input) {
    user {
      ...UserDetails
    }
    token
  }
}
    ${UserDetailsFragmentDoc}`;
export type RegisterMutationFn = Apollo.MutationFunction<RegisterMutation, RegisterMutationVariables>;

/**
 * __useRegisterMutation__
 *
 * To run a mutation, you first call `useRegisterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerMutation, { data, loading, error }] = useRegisterMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useRegisterMutation(baseOptions?: Apollo.MutationHookOptions<RegisterMutation, RegisterMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RegisterMutation, RegisterMutationVariables>(RegisterDocument, options);
      }
export type RegisterMutationHookResult = ReturnType<typeof useRegisterMutation>;
export type RegisterMutationResult = Apollo.MutationResult<RegisterMutation>;
export type RegisterMutationOptions = Apollo.BaseMutationOptions<RegisterMutation, RegisterMutationVariables>;
export const EditUserBasicInfoDocument = gql`
    mutation editUserBasicInfo($input: EditUserBasicInfoInput!) {
  editUserBasicInfo(input: $input) {
    ...UserDetails
  }
}
    ${UserDetailsFragmentDoc}`;
export type EditUserBasicInfoMutationFn = Apollo.MutationFunction<EditUserBasicInfoMutation, EditUserBasicInfoMutationVariables>;

/**
 * __useEditUserBasicInfoMutation__
 *
 * To run a mutation, you first call `useEditUserBasicInfoMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEditUserBasicInfoMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [editUserBasicInfoMutation, { data, loading, error }] = useEditUserBasicInfoMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useEditUserBasicInfoMutation(baseOptions?: Apollo.MutationHookOptions<EditUserBasicInfoMutation, EditUserBasicInfoMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<EditUserBasicInfoMutation, EditUserBasicInfoMutationVariables>(EditUserBasicInfoDocument, options);
      }
export type EditUserBasicInfoMutationHookResult = ReturnType<typeof useEditUserBasicInfoMutation>;
export type EditUserBasicInfoMutationResult = Apollo.MutationResult<EditUserBasicInfoMutation>;
export type EditUserBasicInfoMutationOptions = Apollo.BaseMutationOptions<EditUserBasicInfoMutation, EditUserBasicInfoMutationVariables>;
export const EditUserPasswordDocument = gql`
    mutation editUserPassword($userId: String!, $password: String!, $newPassword: String!) {
  editUserPassword(
    userId: $userId
    password: $password
    newPassword: $newPassword
  ) {
    ...UserDetails
  }
}
    ${UserDetailsFragmentDoc}`;
export type EditUserPasswordMutationFn = Apollo.MutationFunction<EditUserPasswordMutation, EditUserPasswordMutationVariables>;

/**
 * __useEditUserPasswordMutation__
 *
 * To run a mutation, you first call `useEditUserPasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEditUserPasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [editUserPasswordMutation, { data, loading, error }] = useEditUserPasswordMutation({
 *   variables: {
 *      userId: // value for 'userId'
 *      password: // value for 'password'
 *      newPassword: // value for 'newPassword'
 *   },
 * });
 */
export function useEditUserPasswordMutation(baseOptions?: Apollo.MutationHookOptions<EditUserPasswordMutation, EditUserPasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<EditUserPasswordMutation, EditUserPasswordMutationVariables>(EditUserPasswordDocument, options);
      }
export type EditUserPasswordMutationHookResult = ReturnType<typeof useEditUserPasswordMutation>;
export type EditUserPasswordMutationResult = Apollo.MutationResult<EditUserPasswordMutation>;
export type EditUserPasswordMutationOptions = Apollo.BaseMutationOptions<EditUserPasswordMutation, EditUserPasswordMutationVariables>;
export const SingleAvatarUploadDocument = gql`
    mutation singleAvatarUpload($file: Upload!) {
  singleAvatarUpload(file: $file) {
    ...UserDetails
  }
}
    ${UserDetailsFragmentDoc}`;
export type SingleAvatarUploadMutationFn = Apollo.MutationFunction<SingleAvatarUploadMutation, SingleAvatarUploadMutationVariables>;

/**
 * __useSingleAvatarUploadMutation__
 *
 * To run a mutation, you first call `useSingleAvatarUploadMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSingleAvatarUploadMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [singleAvatarUploadMutation, { data, loading, error }] = useSingleAvatarUploadMutation({
 *   variables: {
 *      file: // value for 'file'
 *   },
 * });
 */
export function useSingleAvatarUploadMutation(baseOptions?: Apollo.MutationHookOptions<SingleAvatarUploadMutation, SingleAvatarUploadMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SingleAvatarUploadMutation, SingleAvatarUploadMutationVariables>(SingleAvatarUploadDocument, options);
      }
export type SingleAvatarUploadMutationHookResult = ReturnType<typeof useSingleAvatarUploadMutation>;
export type SingleAvatarUploadMutationResult = Apollo.MutationResult<SingleAvatarUploadMutation>;
export type SingleAvatarUploadMutationOptions = Apollo.BaseMutationOptions<SingleAvatarUploadMutation, SingleAvatarUploadMutationVariables>;