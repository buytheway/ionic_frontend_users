import { IonApp, IonRouterOutlet, IonSplitPane } from "@ionic/react"
import { IonReactRouter } from "@ionic/react-router"
import { Redirect, Route } from "react-router-dom"
import Menu from "./components/Menu"
import Page from "./pages/Page"

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css"

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css"
import "@ionic/react/css/structure.css"
import "@ionic/react/css/typography.css"

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css"
import "@ionic/react/css/float-elements.css"
import "@ionic/react/css/text-alignment.css"
import "@ionic/react/css/text-transformation.css"
import "@ionic/react/css/flex-utils.css"
import "@ionic/react/css/display.css"

/* Theme variables */
import "./theme/variables.css"
import "./theme/global.css"
import MainTabs from "./pages/MainTabs"
import { Login_and_Register } from "./pages/Login_and_Register"
import useAuth from "./hooks/useAuth"
import { GuestGuard } from "./components/GuestGuard"
import { AuthGuard } from "./components/AuthGuard"
import { Profile } from "./pages/Profile"

const App: React.FC = () => {
  const { user, isInitialized, isAuthenticated } = useAuth()

  return (
    <IonApp>
      <IonReactRouter>
        <IonSplitPane contentId="main">
          <Menu />
          <IonRouterOutlet id="main">
            <Route path="/user/profile">
              <AuthGuard>
                <Profile />
              </AuthGuard>
            </Route>
            <Route path="/buy">
              <AuthGuard>
                <MainTabs />
              </AuthGuard>
            </Route>
            <Route path="/page/:name">
              <Page />
            </Route>
            <Route path="/auth/:action">
              <GuestGuard>
                <Login_and_Register />
              </GuestGuard>
            </Route>
            <Route path="/" exact>
              <Redirect to="/buy/home" />
            </Route>
          </IonRouterOutlet>
        </IonSplitPane>
      </IonReactRouter>
    </IonApp>
  )
}

export default App
