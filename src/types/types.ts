export interface IPhoto {
  path: string | undefined
  preview: string | undefined
}
