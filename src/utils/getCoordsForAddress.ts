import axios from "axios"

export const getCoordsForAddress = async (address: string) => {

  let response

  try {
    response = await axios.get(
      `https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURIComponent(
        address
      )}&key=${process.env.REACT_APP_GOOGLE_API_KEY}`
    )
    
  } catch (error) {
    throw new Error('Unable to get a cooridation')
  }

  const data = response.data

  // if (!data || data.status === "ZERO_RESULTS") {
  //   const err = createError(
  //     422,
  //     "Could not find the location for the specified address"
  //   )
  //   throw err
  // }

  console.log("res: " + data.results[0].geometry.location)

  const coordinates: {lat: number, lng: number} = data.results[0].geometry.location

  return coordinates
}
