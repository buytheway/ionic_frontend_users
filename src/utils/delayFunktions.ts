/**
 * delay or pause for some time
 * @param {number} t - time (ms)
 * @return {Promise<*>}
 */
export const delayFunc =  (time: number) =>
  new Promise((resolve) => setTimeout(resolve, time))
