import * as yup from "yup"
import { AnyObject, Maybe } from "yup/lib/types"

yup.addMethod(yup.string, "integer", function () {
  return this.matches(/^\d+$/, "The field should have digits only")
})

declare module "yup" {
  interface StringSchema<
    TType extends Maybe<string> = string | undefined,
    TContext extends AnyObject = AnyObject,
    TOut extends TType = TType
  > extends yup.BaseSchema<TType, TContext, TOut> {
    integer(): StringSchema<TType, TContext>
  }
}

export default yup
