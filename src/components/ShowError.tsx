import { IonText } from "@ionic/react"
import React from "react"

interface Props {
  errorMessage: string
  fontSize?:
    | "medium"
    | "xx-small"
    | "x-small"
    | "small"
    | "large"
    | "x-large"
    | "xx-large"
    | "smaller"
    | "larger"
    | "initial"
    | "inherit"
}

export const ShowError = (props: Props) => {
  if (props.fontSize) {
    return (
      <IonText color="danger" style={{ fontSize: props.fontSize }}>
        {props.errorMessage}
      </IonText>
    )
  }

  return <IonText color="danger">{props.errorMessage}</IonText>
}
