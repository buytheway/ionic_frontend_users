import {
  IonButton,
  IonCol,
  IonItem,
  IonLabel,
  IonList,
  IonRow,
} from "@ionic/react"
import React, { useState } from "react"
import { useForm, SubmitHandler } from "react-hook-form"
import * as yup from "yup"
import useAuth from "../hooks/useAuth"
import { IonInputRHF } from "../utilComponents/IonInputRHF"
import { useYupValidationResolver } from "../utilHooks/useYupValidationResolver"
import { ShowError } from "./ShowError"

interface Props {}

interface IFormInputs {
  username: string
  email: string
  password: string
  confirm_password: string
}

const initialValue = {
  username: "",
  email: "",
  password: "",
  confirm_password: "",
}

const schema = yup.object().shape({
  username: yup.string().min(5).required(),
  email: yup.string().email().required(),
  password: yup.string().min(3).required(),
  confirm_password: yup
    .string()
    .oneOf([yup.ref("password"), null], "Password must match")
    .required(),
})

export const RegisterComp = (props: Props) => {
  const { register: registration } = useAuth()
  const resolver = useYupValidationResolver(schema)
  const [registErr, setRegisterErr] = useState("")

  const { control, register, handleSubmit, reset, formState } =
    useForm<IFormInputs>({
      mode: "onChange",
      resolver: resolver,
      defaultValues: initialValue,
    })

  const registrationHandler: SubmitHandler<IFormInputs> = async (data) => {
    console.log("registration data: ", data)
    setRegisterErr("")
    const res = await registration(data.email, data.username, data.password)

    if (res) {
      setRegisterErr(res)
    }
  }

  return (
    <form noValidate>
      <IonList>
        <IonItem>
          <IonLabel position="stacked" color="primary">
            user name
          </IonLabel>
          <IonInputRHF control={control as any} name="username" type="text" />
        </IonItem>
        <IonItem>
          <IonLabel position="stacked" color="primary">
            Email
          </IonLabel>
          <IonInputRHF
            control={control as any}
            type="email"
            name="email"
            required
          />
        </IonItem>
        <IonItem>
          <IonLabel position="stacked" color="primary">
            Password
          </IonLabel>
          <IonInputRHF
            control={control as any}
            type="password"
            name="password"
          />
        </IonItem>
        <IonItem>
          <IonLabel position="stacked" color="primary">
            Confirm Password
          </IonLabel>
          <IonInputRHF
            control={control as any}
            type="password"
            name="confirm_password"
          />
        </IonItem>
      </IonList>
      <IonRow className="ion-padding-top">
        <IonCol>
          <IonButton
            onClick={(e) => {
              setRegisterErr("")
              reset(initialValue)
            }}
            color="light"
            expand="block"
          >
            Reset
          </IonButton>
        </IonCol>
        <IonCol>
          <IonButton onClick={handleSubmit(registrationHandler)} expand="block">
            Register
          </IonButton>
        </IonCol>
      </IonRow>
      <IonRow>
        <IonCol>
          <ShowError fontSize="smaller" errorMessage={registErr} />
        </IonCol>
      </IonRow>
    </form>
  )
}
