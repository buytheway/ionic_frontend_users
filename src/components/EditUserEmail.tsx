import {
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonRow,
  IonTitle,
  IonToolbar
} from "@ionic/react"
import { useState } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import * as yup from "yup"
import { client, currentUserVar } from "../ac/btwClient"
import { useEditUserEmailMutation, User } from "../generated/graphql"
import useAuth from "../hooks/useAuth"
import { IonInputRHF } from "../utilComponents/IonInputRHF"
import { useYupValidationResolver } from "../utilHooks/useYupValidationResolver"

interface Props {
  user: User
  onDismiss: () => void
}

interface IFormInputs {
  email: string
}

const validationSchema = yup.object({
  email: yup.string().required("Required").email(),
})

export const EditUserEmail = (props: Props) => {
  const { user } = props
  let initialValues = {
    email: user.email,
  }

  const { updateUser } = useAuth()

  const resolver = useYupValidationResolver(validationSchema)

  const [errorMessage, setErrorMessage] = useState<String | null>(null)

  const [editUserEmailMutation] = useEditUserEmailMutation({
    onError: (err) => {
      console.log(err.message)
    },
    client,
  })

  const { control, register, handleSubmit, reset, formState } = useForm<IFormInputs>({
    resolver,
    mode: "onChange",
    defaultValues: initialValues as any,
  })

  const updateHandler: SubmitHandler<IFormInputs> = async (data) => {
    setErrorMessage(null)
    const { data: res, errors } = await editUserEmailMutation({
      variables: { email: data.email },
    })

    if (errors) {
      setErrorMessage(errors.toString())
    }

    if (res?.editUserEmail) {
      updateUser(res.editUserEmail)
      currentUserVar(res.editUserEmail)
      props.onDismiss()
    }

    return
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="end">
            <IonButton expand="block" onClick={props.onDismiss}>
              Close
            </IonButton>
          </IonButtons>
          <IonTitle>Edit Email</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen className="ion-padding">
        <IonRow>
          <IonCol>
            <IonList>
              <IonItem>
                <IonLabel position="stacked" color="primary">
                  email
                </IonLabel>
                <IonInputRHF
                  control={control as any}
                  type="email"
                  name="email"
                  required
                />
              </IonItem>
            </IonList>
          </IonCol>
        </IonRow>

        <IonRow className="ion-padding-top">
          <IonCol>
            <IonButton onClick={(e) => reset()} color="light" expand="block">
              Reset
            </IonButton>
          </IonCol>
          <IonCol>
            <IonButton onClick={handleSubmit(updateHandler)} expand="block" disabled={!formState.isValid}>
              Update
            </IonButton>
          </IonCol>
        </IonRow>
      </IonContent>
    </IonPage>
  )
}
