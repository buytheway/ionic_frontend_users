import { Capacitor } from "@capacitor/core"

import { Camera, CameraResultType, CameraSource } from "@capacitor/camera"

import { IonButton, IonIcon } from "@ionic/react"
import { camera } from "ionicons/icons"
import React, { useRef, useState } from "react"
import "./ImagePicker.css"
import { IPhoto } from "../types/types"

interface Props {
  onImagePicker: (photo: IPhoto) => void
}

export const ImagePicker: React.FC<Props> = (props) => {
  const [takenPhoto, setTakenPhoto] = useState<IPhoto>()

  const filePickerRef = useRef<HTMLInputElement>(null)

  const openFilePicker = () => {
    filePickerRef.current!.click()
  }

  const pickerFileHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target!.files![0]
    const fr = new FileReader()
    fr.onload = () => {
      const photo: IPhoto = {
        path: undefined,
        preview: fr.result!.toString(),
      }
      setTakenPhoto(photo)
      props.onImagePicker(photo)
    }
    fr.readAsDataURL(file)
  }

  const takePhotoHandler = async () => {
    if (!Capacitor.isPluginAvailable("Camera")) {
      console.log("I am unavailable!")
      openFilePicker()
      return
    }

    try {
      const photo = await Camera.getPhoto({
        resultType: CameraResultType.Uri,
        allowEditing: true,
        source: CameraSource.Camera,
        quality: 80,
        width: 500,
      })

      if (!photo || !photo.webPath) {
        return
      }

      const pickedPhoto: IPhoto = {
        path: photo.path,
        preview: photo.webPath,
      }

      setTakenPhoto(pickedPhoto)

      props.onImagePicker(pickedPhoto)
    } catch (error) {
      console.log("I am in Error")
      // openFilePicker()
      openFilePicker()
    }
  }

  return (
    <React.Fragment>
      <div className="image-preview">
        {!takenPhoto && <h3>No photo chosen!</h3>}
        {takenPhoto && <img src={takenPhoto.preview} alt="Preview" />}
      </div>
      <IonButton fill="clear" onClick={takePhotoHandler}>
        <IonIcon icon={camera} slot="start"></IonIcon>
        Take Photo
      </IonButton>
      <input
        type="file"
        hidden
        ref={filePickerRef}
        onChange={pickerFileHandler}
      />
    </React.Fragment>
  )
}
