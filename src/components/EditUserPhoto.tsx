import {
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonRow,
  IonTitle,
  IonToolbar,
  isPlatform,
} from "@ionic/react"
import { v4 as uuidV4 } from "uuid"
import React, { useEffect, useRef, useState } from "react"
import { client, currentUserVar } from "../ac/btwClient"
import { User, useSingleAvatarUploadMutation } from "../generated/graphql"
import useAuth from "../hooks/useAuth"
import { Camera, CameraResultType, CameraSource } from "@capacitor/camera"
import { userInfo } from "os"
import { GUEST_AVATAR } from "../utils/constants"
import { getImageFileFromUrl } from "../utils/imageUtils"
import { gql, useMutation } from "@apollo/client"
import { delay } from "lodash"

// const myMutation = gql`
//   mutation singleUpload($file: Upload!) {
//     singleUpload(file: $file)
//   }
// `

interface Props {
  user: User
  onDismiss: () => void
}

export const EditUserPhoto = (props: Props) => {
  const { updateUser } = useAuth()

  const user = currentUserVar()

  const [avatarUrl, setAvatarUrl] = useState<string | undefined>(
    props.user.avatar as any
  )
  const [avatarFile, setAvatarFile] = useState<File | null>(null)
  const [errorMessage, setErrorMessage] = useState<string>()

  const fileInputRef = useRef<HTMLInputElement>(null)
  const [singleAvatarUploadMutation] = useSingleAvatarUploadMutation({
    onError: (err) => {
      console.log(err.message)
    },
    client,
  })

  // const [myUpload] = useMutation(myMutation, {
  //   onError: (err) => {
  //     console.log(err.message)
  //   },
  //   onCompleted: (data) => {
  //     console.log("data", data)
  //   },
  //   client,
  // })

  const handleAvatarClick = async () => {
    if (isPlatform("capacitor")) {
      try {
        const photo = await Camera.getPhoto({
          resultType: CameraResultType.Uri,
          source: CameraSource.Prompt,
        })

        console.log("avatarUrl: ", avatarUrl)

        const myUrl = photo.webPath ? photo.webPath : GUEST_AVATAR

        setAvatarUrl(myUrl)

        const fileName = myUrl.substring(myUrl.lastIndexOf("/") + 1)

        const imageFile = await getImageFileFromUrl(myUrl, fileName)

        // TODO: hier get avatar file path and set the avatarFile

        console.log("selected Image WebPath:", photo.webPath)
        console.log("selected Image Path:", photo.path)

        setAvatarFile(imageFile)
      } catch (error) {
        console.log("Camera error: ", error)
      }
    } else {
      fileInputRef.current?.click()
    }
  }

  const handleAvatarChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.files && event.target.files?.length > 0) {
      const file = event.target.files[0]
      setAvatarUrl(URL.createObjectURL(file))
      setAvatarFile(file)
    }
  }

  const updateAvatarHandler = async () => {
    setErrorMessage(undefined)

    if (!!!avatarFile) {
      console.log("avatarFile doesn't existiert!!!")
      return
    }

    console.log("avatarFile: ", avatarFile)

    const { data: res, errors } = await singleAvatarUploadMutation({
      variables: { file: avatarFile },
    })

    // const { data: res, errors } = await myUpload({
    //   variables: { file: avatarFile },
    // })

    if (errors) {
      setErrorMessage(errors.toString())
    }

    if (res?.singleAvatarUpload) {
      console.log("I am uploaded..., result is: ")
      console.log(res.singleAvatarUpload.avatar)

      setTimeout(() => {
        updateUser(res.singleAvatarUpload)
        currentUserVar(res.singleAvatarUpload)
        props.onDismiss()
      }, 1000)
    }
  }

  useEffect(
    () => () => {
      if (avatarUrl && avatarUrl.startsWith("blob:")) {
        URL.revokeObjectURL(avatarUrl)
      }
    },
    [avatarUrl]
  )

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="end">
            <IonButton expand="block" onClick={props.onDismiss}>
              close
            </IonButton>
          </IonButtons>
          <IonTitle>Edit Avatar</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen className="ion-padding">
        <IonRow>
          <IonCol>
            <IonList>
              <IonItem>
                <IonLabel position="stacked" color="primary">
                  username (not changable)
                </IonLabel>
                <IonInput type="text" disabled>
                  {currentUserVar()?.username}
                </IonInput>
              </IonItem>
              <IonItem>
                <IonLabel position="stacked" color="primary">
                  Avatar
                </IonLabel>
                <br />
                <input
                  type="file"
                  accept="image/*"
                  onChange={handleAvatarChange}
                  ref={fileInputRef}
                  hidden
                />
                <img
                  src={avatarUrl}
                  alt="click here to upload file"
                  onClick={handleAvatarClick}
                  style={{ cursor: "pointer" }}
                />
              </IonItem>
            </IonList>
          </IonCol>
        </IonRow>
        <IonRow className="ion-padding-top ion-padding-bottom">
          <IonCol>
            <IonButton
              onClick={(e) => {
                setAvatarUrl(props.user.avatar!)
                setAvatarFile(null)
              }}
              color="light"
              expand="block"
            >
              Reset
            </IonButton>
          </IonCol>
          <IonCol>
            <IonButton onClick={() => updateAvatarHandler()} expand="block">
              Update
            </IonButton>
          </IonCol>
        </IonRow>
      </IonContent>
    </IonPage>
  )
}
