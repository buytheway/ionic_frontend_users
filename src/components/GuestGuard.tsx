import React, { Children, FC, ReactNode, useContext, useEffect } from "react"
import useAuth from "../hooks/useAuth"
import PropTypes from "prop-types"
import { NavContext, useIonViewWillEnter, IonRedirect } from "@ionic/react"
import { Redirect } from "react-router"

interface Props {
  children: ReactNode
}

export const GuestGuard: FC<Props> = (props: Props) => {
  const auth = useAuth()
  const { navigate } = useContext(NavContext)

  // useIonViewWillEnter(() => {
  //   if (!auth.isInitialized) {
  //     console.log("GuestGuard isInitialized", auth.isInitialized)
  //     return
  //   }

  //   if (auth.isAuthenticated) {
  //     navigate("/page/spam")
  //   }
  // }, [auth.isAuthenticated, auth.isInitialized])

  if (!auth.isInitialized) {
    console.log("isInitialized: ", auth.isInitialized)
    return <></>
  }

  if (auth.isAuthenticated) {
    return <Redirect to="/buy/home" />
  }

  return <>{props.children}</>
}

GuestGuard.propTypes = {
  children: PropTypes.node,
}
