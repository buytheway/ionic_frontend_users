import {
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonPage,
  IonRow,
  IonText,
  IonTitle,
  IonToolbar,
} from "@ionic/react"
import React, { useState } from "react"
import {
  appendErrors,
  SubmitErrorHandler,
  SubmitHandler,
  useForm,
} from "react-hook-form"
import { client } from "../ac/btwClient"
import { useEditUserPasswordMutation } from "../generated/graphql"
import { IonInputRHF } from "../utilComponents/IonInputRHF"
import { useYupValidationResolver } from "../utilHooks/useYupValidationResolver"
import yup from "../utils/yupExtended"

interface Props {
  userId: string
  onDismiss: () => void
}

interface IFormInputs {
  current_password: string
  new_password: string
  confirm_newPassword: string
}

const intialValue = {
  current_password: "",
  new_password: "",
  confirm_new_password: "",
}

const schema = yup.object().shape({
  current_password: yup.string().min(3).required(),
  new_password: yup.string().min(3).required(),
  confirm_new_password: yup
    .string()
    .min(3)
    .required()
    .oneOf([yup.ref("new_password"), null], "Password must match"),
})

export const EditUserPassword = (props: Props) => {
  
  const [changePasswordErr, setChangePasswordErr] = useState("")

  const [editUserPasswordMutation] = useEditUserPasswordMutation({
    onError: (err) => {
      console.log(err.message)
    },
    client,
  })

  const updatePasswordHandler: SubmitHandler<IFormInputs> = async (data) => {
    console.log(data)
    setChangePasswordErr("")
    const { data: res, errors } = await editUserPasswordMutation({
      variables: {
        userId: props.userId,
        password: data.current_password,
        newPassword: data.new_password,
      },
    })
    if (errors) {
      setChangePasswordErr(errors.toString())
    } else {
      props.onDismiss()
    }
  }

  const resolver = useYupValidationResolver(schema)

  const { control, register, handleSubmit, reset, formState } =
    useForm<IFormInputs>({
      mode: "onChange",
      resolver: resolver,
      defaultValues: intialValue,
    })

  const onErrorHandler: SubmitErrorHandler<IFormInputs> = (err) => {
    console.log(err)
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="end">
            <IonButton expand="block" onClick={props.onDismiss}>
              Close
            </IonButton>
          </IonButtons>
          <IonTitle>Edit Password</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen className="ion-padding">
        <IonRow>
          <IonCol>
            <IonItem>
              <IonLabel position="floating" color="primary">
                current password
              </IonLabel>
              <IonInputRHF
                control={control as any}
                name="current_password"
                type="text"
              />
            </IonItem>
            <IonItem>
              <IonLabel position="floating" color="primary">
                new password
              </IonLabel>
              <IonInputRHF
                control={control as any}
                name="new_password"
                type="text"
              />
            </IonItem>
            <IonItem>
              <IonLabel position="floating" color="primary">
                confirm new password
              </IonLabel>
              <IonInputRHF
                control={control as any}
                name="confirm_new_password"
                type="text"
              />
            </IonItem>
          </IonCol>
        </IonRow>
        {!!changePasswordErr && (
          <IonRow>
            <IonCol>
              <IonText color="danger">{changePasswordErr}</IonText>
            </IonCol>
          </IonRow>
        )}
        <IonRow className="ion-padding-top">
          <IonCol>
            <IonButton
              expand="block"
              color="light"
              onClick={(e) => {
                setChangePasswordErr("")
                reset()
              }}
            >
              Reset
            </IonButton>
          </IonCol>
          <IonCol>
            <IonButton
              expand="block"
              onClick={handleSubmit(updatePasswordHandler)}
              disabled={!formState.isValid}
            >
              Update
            </IonButton>
          </IonCol>
        </IonRow>
      </IonContent>
    </IonPage>
  )
}
