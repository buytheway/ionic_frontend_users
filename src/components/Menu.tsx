import {
  IonButton,
  IonCol,
  IonContent,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenu,
  IonMenuToggle,
  IonNote,
  IonRow,
} from "@ionic/react"

import { useLocation } from "react-router-dom"
import {
  archiveOutline,
  archiveSharp,
  bookmarkOutline,
  heartOutline,
  heartSharp,
  mailOutline,
  mailSharp,
  paperPlaneOutline,
  paperPlaneSharp,
  trashOutline,
  trashSharp,
  warningOutline,
  warningSharp,
  homeOutline,
  homeSharp,
  logInOutline,
  logInSharp,
  personOutline,
  personSharp,
  settingsOutline,
  settingsSharp,
  ribbonOutline,
  ribbonSharp,
  locationOutline,
  locationSharp,
} from "ionicons/icons"
import "./Menu.css"
import useAuth from "../hooks/useAuth"
import React, { useEffect } from "react"
import { useReactiveVar } from "@apollo/client"
import { currentUserVar } from "../ac/btwClient"

interface AppPage {
  url: string
  iosIcon: string
  mdIcon: string
  title: string
}

const appPages: AppPage[] = [
  {
    title: "Buy the way",
    url: "/",
    iosIcon: homeOutline,
    mdIcon: homeSharp,
  },
  {
    title: "Locations",
    url: "/user/locations",
    iosIcon: locationOutline,
    mdIcon: locationSharp,
  },

  {
    title: "Points",
    url: "/user/points",
    iosIcon: ribbonOutline,
    mdIcon: ribbonSharp,
  },
  {
    title: "Profile",
    url: "/user/profile",
    iosIcon: personOutline,
    mdIcon: personSharp,
  },
  {
    title: "Settings",
    url: "/user/settings",
    iosIcon: settingsOutline,
    mdIcon: settingsSharp,
  },
  // {
  //   title: "Outbox",
  //   url: "/page/Outbox",
  //   iosIcon: paperPlaneOutline,
  //   mdIcon: paperPlaneSharp,
  // },
  // {
  //   title: "Favorites",
  //   url: "/page/Favorites",
  //   iosIcon: heartOutline,
  //   mdIcon: heartSharp,
  // },
  // {
  //   title: "Archived",
  //   url: "/page/Archived",
  //   iosIcon: archiveOutline,
  //   mdIcon: archiveSharp,
  // },
  // {
  //   title: "Trash",
  //   url: "/page/Trash",
  //   iosIcon: trashOutline,
  //   mdIcon: trashSharp,
  // },
  // {
  //   title: "Spam",
  //   url: "/page/Spam",
  //   iosIcon: warningOutline,
  //   mdIcon: warningSharp,
  // },
  {
    title: "Login / Register",
    url: "/auth/login",
    iosIcon: logInOutline,
    mdIcon: logInSharp,
  },
]

const labels = ["Family", "Friends", "Notes", "Work", "Travel", "Reminders"]

const Menu: React.FC = () => {
  const location = useLocation()

  const { logout } = useAuth()

  const currentUser = useReactiveVar(currentUserVar)

  return (
    <IonMenu contentId="main" type="overlay">
      <IonContent>
        <IonList id="inbox-list">
          <IonListHeader>
            {currentUser ? currentUser.username : "Guest"}
          </IonListHeader>
          <IonNote>{currentUser?.email}</IonNote>
          {appPages.map((appPage, index) => {
            return (
              <IonMenuToggle key={index} autoHide={false}>
                <IonItem
                  className={
                    location.pathname === appPage.url ? "selected" : ""
                  }
                  routerLink={appPage.url}
                  routerDirection="none"
                  lines="none"
                  detail={false}
                >
                  <IonIcon
                    slot="start"
                    ios={appPage.iosIcon}
                    md={appPage.mdIcon}
                  />
                  <IonLabel>{appPage.title}</IonLabel>
                </IonItem>
              </IonMenuToggle>
            )
          })}
        </IonList>

        {currentUser && (
          <IonRow className="ion-padding-top">
            <IonCol>
              <IonButton expand="block" onClick={logout}>
                Logout
              </IonButton>
            </IonCol>
          </IonRow>
        )}
        <IonList id="labels-list">
          <IonListHeader>Labels</IonListHeader>
          {labels.map((label, index) => (
            <IonItem lines="none" key={index}>
              <IonIcon slot="start" icon={bookmarkOutline} />
              <IonLabel>{label}</IonLabel>
            </IonItem>
          ))}
        </IonList>
      </IonContent>
    </IonMenu>
  )
}

export default Menu
