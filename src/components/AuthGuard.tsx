import React, { ReactNode } from "react"
import useAuth from "../hooks/useAuth"
import PropTypes from "prop-types"
import { Redirect } from "react-router"

interface Props {
  children: ReactNode
}

export const AuthGuard: React.FC<Props> = (props) => {
  const auth = useAuth()

  console.log("AuthGuard auth: ", auth)

  if (!auth.isInitialized) {
    return <div>AuthGuard loading...</div>
  }

  if (!auth.isAuthenticated) {
    return <Redirect to="/auth/login"></Redirect>
  }

  return <>{props.children}</>
}

AuthGuard.propTypes = {
  children: PropTypes.node,
}
