import {
  IonButton,
  IonCol,
  IonItem,
  IonLabel,
  IonList,
  IonRow,
  IonText,
} from "@ionic/react"
import React, { useState } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import * as yup from "yup"
import useAuth from "../hooks/useAuth"
import { IonInputRHF } from "../utilComponents/IonInputRHF"
import { useYupValidationResolver } from "../utilHooks/useYupValidationResolver"
import { ShowError } from "./ShowError"
interface Props {}

interface IFormInputs {
  email: string
  password: string
}

let initialValues = {
  email: "",
  password: "",
}

const validationSchema = yup.object({
  email: yup.string().required("Required").email(),
  password: yup.string().required("Required").min(3),
})
export const LoginComp = (props: Props) => {
  const { login } = useAuth()
  const resolver = useYupValidationResolver(validationSchema)
  const [loginErr, setLoginErr] = useState("")

  const {
    register,
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<IFormInputs>({
    resolver,
    mode: "onChange",
    defaultValues: initialValues,
  })

  const loginHandler: SubmitHandler<IFormInputs> = async (data) => {
    console.log("data: ", data)
    setLoginErr("")
    const res = await login(data.email, data.password)
    if (res) {
      setLoginErr(res)
    }
  }

  /**
   *
   * @param _fieldName
   */
  const showError = (_fieldName: keyof IFormInputs) => {
    return (
      errors[_fieldName] && (
        <div
          style={{
            color: "red",
            padding: 5,
            paddingLeft: 12,
            fontSize: "smaller",
          }}
        >
          {_fieldName}:{errors[_fieldName]!.message || "This field is required"}
        </div>
      )
    )
  }
  return (
    <form noValidate>
      <IonList>
        <IonItem>
          <IonLabel position="stacked" color="primary">
            Email
          </IonLabel>
          <IonInputRHF
            control={control as any}
            type="email"
            name="email"
            required
          />
        </IonItem>

        <IonItem>
          <IonLabel position="stacked" color="primary">
            Password
          </IonLabel>
          <IonInputRHF
            name="password"
            type="password"
            control={control as any}
          />
        </IonItem>
      </IonList>

      <IonRow>
        <IonCol>
          <IonButton
            onClick={(e) => {
              setLoginErr("")
              reset(initialValues)
            }}
            expand="block"
            color="light"
          >
            Reset
          </IonButton>
        </IonCol>
        <IonCol>
          <IonButton expand="block" onClick={handleSubmit(loginHandler)}>
            Login
          </IonButton>
        </IonCol>
      </IonRow>
      {!!loginErr && (
        <IonRow>
          <IonCol>
            <ShowError fontSize="smaller" errorMessage={loginErr} />
          </IonCol>
        </IonRow>
      )}
      <IonRow className="ion-padding-top">
        <IonCol>
          Forget your password? Then please click here to reset your password.
        </IonCol>
      </IonRow>
    </form>
  )
}
