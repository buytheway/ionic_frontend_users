import {
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonRow,
  IonTitle,
  IonToolbar
} from "@ionic/react"
import GoogleMapReact from "google-map-react"
import { locationSharp } from "ionicons/icons"
import _ from "lodash"
import React, { useState } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import { client, currentUserVar } from "../ac/btwClient"
import { useEditUserBasicInfoMutation, User } from "../generated/graphql"
import useAuth from "../hooks/useAuth"
import { IonInputRHF } from "../utilComponents/IonInputRHF"
import { useYupValidationResolver } from "../utilHooks/useYupValidationResolver"
import { getCoordsForAddress } from "../utils/getCoordsForAddress"
import yup from "../utils/yupExtended"

const MarkerComponent: React.FC<{ lat: number; lng: number }> = () => (
  <IonIcon icon={locationSharp} color="danger" size="large"></IonIcon>
)

interface Props {
  user: User
  onDismiss: () => void
}

interface IFormInputs {
  tel?: string | null
  address?: string | null
  zip?: string | null
  city?: string | null
  country?: string | null
  gps?: Number[] | null
}


const validationSchema = yup.object({
  tel: yup.string().required(),
  address: yup.string().required(),
  zip: yup.string().integer().required(),
  city: yup.string().required(),
  country: yup.string().required(),
})

export const EditBasicInfo = (props: Props) => {
  const { user } = props

  const { updateUser } = useAuth()

  const [gps, setGps] = useState<number[] | undefined | null>(user.gps as any)

  const [errorMessage, setErrorMessage] = useState<String | null>(null)

  const [editUserBasicInfoMutation] = useEditUserBasicInfoMutation({
    onError: (err) => {
      console.log(err.message)
    },
    client,
  })

  const resolver = useYupValidationResolver(validationSchema)

  const { control, handleSubmit, reset, watch, formState } =
    useForm<IFormInputs>({
      resolver,
      mode: "onChange",
      defaultValues: {
        tel: user.tel,
        address: user.address,
        zip: user.zip,
        city: user.city,
        country: user.country,
        gps: user.gps,
      },
    })

  const watchAddress = () => {
    let res = watch()
    res = _.omit(res, ["tel", "gps"])
    let address
    const all_fileds_filled =
      !!res.address && !!res.zip && !!res.city && !!res.country

    if (all_fileds_filled) {
      address = `${res.address}, ${res.zip} ${res.city}, ${res.country}`
    }

    return address
  }

  const getLocation = async () => {
    const address = watchAddress()
    let res
    if (address) {
      res = await getCoordsForAddress(address)
      setGps([res.lat, res.lng])
    }
  }

  const updateHandler: SubmitHandler<IFormInputs> = async (data) => {
    setErrorMessage(null)
    const { data: res, errors } = await editUserBasicInfoMutation({
      variables: {
        input: {
          tel: data.tel!,
          address: data.address!,
          zip: data.zip!,
          city: data.city!,
          country: data.country!,
        },
      },
    })

    if (errors) {
      setErrorMessage(errors.toString())
    }

    if (res?.editUserBasicInfo) {
      updateUser(res.editUserBasicInfo)
      currentUserVar(res.editUserBasicInfo)
      props.onDismiss()
    }
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="end">
            <IonButton expand="block" onClick={props.onDismiss}>
              Close
            </IonButton>
          </IonButtons>
          <IonTitle>Edit Basic Infos</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen className="ion-padding">
        <IonRow>
          <IonCol>
            <IonList>
              <IonItem>
                <IonLabel position="floating" color="primary">
                  tel
                </IonLabel>
                <IonInputRHF control={control as any} type="text" name="tel" />
              </IonItem>
              <IonItem>
                <IonLabel position="floating" color="primary">
                  address
                </IonLabel>
                <IonInputRHF
                  control={control as any}
                  type="text"
                  name="address"
                />
              </IonItem>
              <IonItem>
                <IonLabel position="floating" color="primary">
                  zip
                </IonLabel>
                <IonInputRHF control={control as any} type="text" name="zip" />
              </IonItem>
              <IonItem>
                <IonLabel position="floating" color="primary">
                  city
                </IonLabel>
                <IonInputRHF control={control as any} type="text" name="city" />
              </IonItem>

              <IonItem>
                <IonLabel position="floating" color="primary">
                  country
                </IonLabel>
                <IonInputRHF
                  control={control as any}
                  type="text"
                  name="country"
                />
              </IonItem>
            </IonList>
          </IonCol>
        </IonRow>
        <IonRow>
          <IonCol className="ion-text-center">
            <IonButton
              fill="clear"
              onClick={getLocation}
              disabled={!!!watchAddress()}
            >
              get location
            </IonButton>
          </IonCol>
        </IonRow>
        {!!gps && (
          <IonRow className="myLocation ion-justify-content-center">
            <IonCol size="12">
              <GoogleMapReact
                bootstrapURLKeys={{
                  key: process.env.REACT_APP_GOOGLE_API_KEY as string,
                }}
                center={{ lat: gps[0], lng: gps[1] }}
                defaultZoom={14}
              >
                <MarkerComponent lat={gps[0]} lng={gps[1]}></MarkerComponent>
              </GoogleMapReact>
            </IonCol>
          </IonRow>
        )}
        <IonRow className="ion-padding-top ion-padding-bottom">
          <IonCol>
            <IonButton onClick={() => reset()} color="light" expand="block">
              Reset
            </IonButton>
          </IonCol>
          <IonCol>
            <IonButton
              onClick={handleSubmit(updateHandler)}
              expand="block"
              disabled={!formState.isValid}
            >
              Update
            </IonButton>
          </IonCol>
        </IonRow>
      </IonContent>
    </IonPage>
  )
}
