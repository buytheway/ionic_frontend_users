import { UseControllerProps, useController } from "react-hook-form"
import { IonInput, IonRow, IonCol, IonText } from "@ionic/react"
import styles from "./IonInputRHF.module.scss"

interface IonInputRHFProps extends UseControllerProps {
  type: string
  placeholder?: string
  required?: boolean
  children?: React.ReactChild | React.ReactChildren
  className?: string | undefined
}

export const IonInputRHF: React.FC<IonInputRHFProps> = (props) => {
  const { field, fieldState } = useController(props)

  return (
    <>
      <IonInput
        {...field}
        type={props.type as any}
        onIonChange={field.onChange}
        placeholder={props.placeholder}
        className={props.className}
      >
        {props.children}
      </IonInput>
      {/*       <p>{fieldState.isTouched && "Touched"}</p>
      <p>{fieldState.isDirty && "Dirty"}</p> */}

      <IonText color="danger" className={styles.error_size}>
        {fieldState.invalid &&
          fieldState.isTouched &&
          fieldState.error?.message}
      </IonText>
    </>
  )
}
