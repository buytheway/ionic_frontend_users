import { createContext, useEffect, useReducer } from "react"
import type { FC, ReactNode } from "react"
import PropTypes from "prop-types"
import { useEditUserEmailMutation, User } from "../generated/graphql"
import {
  useCurrentUserQuery,
  useLoginMutation,
  useRegisterMutation,
} from "../generated/graphql"
import { Storage } from "@capacitor/storage"
import { ACCESSTOKEN } from "../utils/constants"
import { useForceUpdate } from "../utilHooks/useForceUpdate"
import { currentUserVar } from "../ac/btwClient"
interface State {
  isInitialized: boolean
  isAuthenticated: boolean
  user: User | null
}

interface AuthContextValue extends State {
  platform: "JWT"
  login: (email: string, password: string) => Promise<string | null>
  logout: () => Promise<void>
  register: (
    email: string,
    username: string,
    password: string
  ) => Promise<string | null>
  updateUser: (user: User) => void
}

interface AuthProviderProps {
  children: ReactNode
}

type InitializeAction = {
  type: "INITIALIZE"
  payload: {
    isAuthenticated: boolean
    user: User | null
  }
}

type LoginAction = {
  type: "LOGIN"
  payload: {
    user: User
  }
}

type UpdateUserAction = {
  type: "UPDATEUSER"
  payload: {
    user: User
  }
}

type LogoutAction = {
  type: "LOGOUT"
}

type RegisterAction = {
  type: "REGISTER"
  payload: {
    user: User
  }
}

type Action =
  | InitializeAction
  | LoginAction
  | LogoutAction
  | RegisterAction
  | UpdateUserAction

const initialState: State = {
  isAuthenticated: false,
  isInitialized: false,
  user: null,
}

const handlers: Record<string, (state: State, action: any) => State> = {
  INITIALIZE: (state: State, action: InitializeAction): State => {
    const { isAuthenticated, user } = action.payload

    return {
      ...state,
      isAuthenticated,
      isInitialized: true,
      user,
    }
  },
  LOGIN: (state: State, action: LoginAction): State => {
    const { user } = action.payload

    return {
      ...state,
      isAuthenticated: true,
      user,
    }
  },
  LOGOUT: (state: State): State => ({
    ...state,
    isAuthenticated: false,
    user: null,
  }),
  REGISTER: (state: State, action: RegisterAction): State => {
    const { user } = action.payload

    return {
      ...state,
      isAuthenticated: true,
      user,
    }
  },
  UPDATEUSER: (state: State, action: UpdateUserAction): State => {
    const { user } = action.payload

    const res = {
      ...state,
      user,
    }

    console.log("updated user State: ", res)

    return res
  },
}

const reducer = (state: State, action: Action): State =>
  handlers[action.type] ? handlers[action.type](state, action) : state

const AuthContext = createContext<AuthContextValue>({
  ...initialState,
  platform: "JWT",
  login: () => Promise.resolve() as any,
  logout: () => Promise.resolve(),
  register: () => Promise.resolve() as any,
  updateUser: (user: User) => null,
})

export const AuthProvider: FC<AuthProviderProps> = (props) => {
  const { children } = props
  const [state, dispatch] = useReducer(reducer, initialState)
  const {
    loading,
    error: currentUserErr,
    data,
  } = useCurrentUserQuery({
    onError: (err) => {
      console.log(err)
    },
  })

  const forceUpdate = useForceUpdate()

  const me = data?.currentUser?.id

  const [loginMutation] = useLoginMutation({
    onError: (err) => {
      console.log(err.message)
    },
  })
  const [registrationMutation] = useRegisterMutation({
    onError: (err) => {
      console.log(err.message)
    },
  })

  useEffect(() => {
    const initialize = async () => {
      const { value: accessToken } = await Storage.get({ key: ACCESSTOKEN })
      if (!accessToken || currentUserErr) {
        Storage.clear()
        dispatch({
          type: "INITIALIZE",
          payload: {
            isAuthenticated: false,
            user: null,
          },
        })
        currentUserVar(null)

        return
      }

      console.log("isLoading: ", loading)
      console.log("JWT user data: ", data)
      if (loading) {
        return
      }

      const user = data?.currentUser

      if (!!!user) {
        dispatch({
          type: "INITIALIZE",
          payload: {
            isAuthenticated: false,
            user: null,
          },
        })
        currentUserVar(null)
      } else {
        dispatch({
          type: "INITIALIZE",
          payload: {
            isAuthenticated: true,
            user: user,
          },
        })
        currentUserVar(user)
      }
    }

    initialize()
  }, [me])

  // if (loading) return <div>JWT Loading...</div>

  const login = async (
    email: string,
    password: string
  ): Promise<string | null> => {
    const res = await loginMutation({ variables: { email, password } })
    const { data: loginMutationData, errors: loginMutationError } = res
    if (loginMutationError) {
      console.log("loginMutationData", loginMutationData)
      console.log("login error is: ", loginMutationError.toString())
      return loginMutationError.toString()
    }

    if (!loginMutationData) {
      return "something wrong at login..."
    }

    const user = loginMutationData!.login.user!

    const accessToken = loginMutationData!.login.token!

    await Storage.set({ key: ACCESSTOKEN, value: accessToken })

    dispatch({
      type: "LOGIN",
      payload: {
        user,
      },
    })

    currentUserVar(user)

    return null
  }

  const logout = async (): Promise<void> => {
    Storage.clear()
    dispatch({ type: "LOGOUT" })
    currentUserVar(null)
  }

  const register = async (
    email: string,
    username: string,
    password: string
  ): Promise<string | null> => {
    const {
      data: registrationMutationData,
      errors: registrationMutationError,
    } = await registrationMutation({
      variables: { input: { email, username, password } },
    })

    if (registrationMutationError) {
      console.log("regist error!")
      return registrationMutationError.toString()
    }

    const accessToken = registrationMutationData!.register.token!

    const user = registrationMutationData!.register.user!

    Storage.set({ key: ACCESSTOKEN, value: accessToken })

    dispatch({
      type: "REGISTER",
      payload: {
        user,
      },
    })
    currentUserVar(user)
    return null
  }

  const updateUser = (user: User) => {
    dispatch({
      type: "UPDATEUSER",
      payload: {
        user,
      },
    })
    currentUserVar(user)
  }

  return (
    <AuthContext.Provider
      value={{
        ...state,
        platform: "JWT",
        login,
        logout,
        register,
        updateUser,
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}

AuthProvider.propTypes = {
  children: PropTypes.node.isRequired,
}

export default AuthContext
