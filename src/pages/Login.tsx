import {
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonMenuButton,
  IonPage,
  IonRow,
  IonTitle,
  IonToolbar,
} from "@ionic/react"
import React, { useState } from "react"
import useAuth from "../hooks/useAuth"
import "./Login.scss"
import { DevTool } from "@hookform/devtools"

interface LoginProps {}

//Note: hamburg menu should not show: done with disable the IonMenuButton  in the <IonToolbar>
// TODO Please finish the apollo graphql template
// TODO Please finish the apollo graphql login page
const Login: React.FC<LoginProps> = (props) => {
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const { login } = useAuth()



  const loginHandler = async (e: React.FormEvent) => {
    e.preventDefault()

    await login(email, password)
  }

  return (
    <IonPage id="login-page">
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Login</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <div className="login-logo">
          <img src="assets/img/btw_logo.png" alt="buy the way logo" />
        </div>
        <form noValidate>
          <IonList>
            <IonItem>
              <IonLabel position="stacked" color="primary">
                Email
              </IonLabel>
              <IonInput
                name="email"
                type="email"
                value={email}
                spellCheck={false}
                autoCapitalize="off"
                placeholder="xirururu@gmail.com"
                onIonChange={(e) => setEmail(e.detail.value!)}
                required
              />
            </IonItem>

            <IonItem>
              <IonLabel position="stacked" color="primary">
                Password
              </IonLabel>
              <IonInput
                name="password"
                type="password"
                value={password}
                placeholder="123"
                onIonChange={(e) => setPassword(e.detail.value!)}
              />
            </IonItem>
          </IonList>

          <IonRow>
            <IonCol>
              <IonButton expand="block" onClick={loginHandler}>
                Login
              </IonButton>
            </IonCol>
            <IonCol>
              <IonButton routerLink="/signup" color="light" expand="block">
                Signup
              </IonButton>
            </IonCol>
          </IonRow>
        </form>
      </IonContent>
    </IonPage>
  )
}

export default Login
