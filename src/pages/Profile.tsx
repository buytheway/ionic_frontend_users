import { useReactiveVar } from "@apollo/client"
import {
  IonAvatar,
  IonBackButton,
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonListHeader,
  IonPage,
  IonRow,
  IonText,
  IonTitle,
  IonToolbar,
  useIonModal
} from "@ionic/react"
import { createOutline } from "ionicons/icons"
import { useReducer, useState } from "react"
import { currentUserVar } from "../ac/btwClient"
import { EditBasicInfo } from "../components/EditBasicInfo"
import { EditUserEmail } from "../components/EditUserEmail"
import { EditUserPassword } from "../components/EditUserPassword"
import { EditUserPhoto } from "../components/EditUserPhoto"

interface Props {}

const initialModalState = {
  editBasicInfo: false,
  changePassword: false,
  editBankAccount: false,
}

type modalActionKind =
  | "openEditBasicInfo"
  | "openChangePassword"
  | "openEditBankAccount"
  | "closeAll"

const modalStateReducer = (
  state: typeof initialModalState,
  action: { type: modalActionKind }
) => {
  switch (action.type) {
    case "openEditBasicInfo":
      return { ...initialModalState, editBasicInfo: true }

    case "openChangePassword":
      return { ...initialModalState, changePassword: true }

    case "openEditBankAccount":
      return { ...initialModalState, editBankAccount: true }

    case "closeAll":
      return { ...initialModalState }
  }
}

export const Profile = (props: Props) => {
  // const { user } = useAuth()
  const user = useReactiveVar(currentUserVar)

  const [gps, setGps] = useState<{ lat: number; lng: number } | null>(null)

  const [state, dispatch] = useReducer(modalStateReducer, initialModalState)

  const closeEditUserEmailModalHandler = () => {
    dismiss_editUserEmail()
  }

  const [present_editUserEmail, dismiss_editUserEmail] = useIonModal(
    EditUserEmail,
    {
      user,
      onDismiss: closeEditUserEmailModalHandler,
    }
  )

  const closeEditUserPhotoModalHandler = () => {
    dismiss_editUserPhoto()
  }

  const [present_editUserPhoto, dismiss_editUserPhoto] = useIonModal(
    EditUserPhoto,
    { user,
      onDismiss: closeEditUserPhotoModalHandler,
    }
  )

  const closeEditBasicInfoModalHandler = () => {
    dismiss_editBasicInfo()
  }

  const [present_editBasicInfo, dismiss_editBasicInfo] = useIonModal(
    EditBasicInfo,
    {
      user,
      onDismiss: closeEditBasicInfoModalHandler,
    }
  )

  const closeEditPasswordHandler = () => {
    dismiss_editUserPassword()
  }

  const [present_editUserPassword, dismiss_editUserPassword] = useIonModal(
    EditUserPassword,
    {
      userId: user!.id,
      onDismiss: closeEditPasswordHandler,
    }
  )

  if (!user) {
    return <div>Loading profile...</div>
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/buy/home" />
          </IonButtons>
          <IonTitle>Profile</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen className="ion-align-items-center">
        <IonGrid>
          <IonRow className="ion-padding-top ion-justify-content-center">
            <IonCol size="auto">
              <IonAvatar style={{ width: "5rem", height: "5rem" }}>
                <img src={user ? user!.avatar! : "assets/img/profile.jpg"} />
                {/* <IonBadge
                  style={{
                    position: "absolute",
                    bottom: "-0.2rem",
                    right: "-1.5rem",
                  }}
                  color="light"
                >
                  Edit
                </IonBadge> */}
                <IonButton
                  onClick={() => present_editUserPhoto()}
                  style={{
                    position: "absolute",
                    right: "-2.2rem",
                    bottom: "-1rem",
                  }}
                  fill="clear"
                >
                  <IonIcon icon={createOutline}></IonIcon>
                </IonButton>
              </IonAvatar>
            </IonCol>
          </IonRow>
          <IonRow className="ion-justify-content-center">
            <IonText color="primary">
              <h4>{user.username}</h4>
            </IonText>
          </IonRow>

          <IonListHeader mode="ios">
            <IonLabel>Konto</IonLabel>
            <IonButton onClick={() => present_editUserEmail()}>
              <IonIcon icon={createOutline}></IonIcon>
            </IonButton>
          </IonListHeader>
          <IonItem lines="none">
            <IonLabel position="fixed">Email</IonLabel>
            <IonText>{user.email}</IonText>
          </IonItem>

          <IonListHeader mode="ios">
            <IonLabel>Basic Info</IonLabel>
            <IonButton onClick={() => present_editBasicInfo()}>
              <IonIcon icon={createOutline}></IonIcon>
            </IonButton>
          </IonListHeader>

          <IonItem lines="none">
            <IonLabel position="fixed">Tel</IonLabel>
            <IonText>{user.tel}</IonText>
          </IonItem>
          <IonItem lines="none">
            <IonLabel position="fixed">Address</IonLabel>
            <IonText>{user.address}</IonText>
          </IonItem>
          <IonItem lines="none">
            <IonLabel position="fixed">Zip</IonLabel>
            <IonText>{user.zip}</IonText>
          </IonItem>
          <IonItem lines="none">
            <IonLabel position="fixed">City</IonLabel>
            <IonText>{user.city}</IonText>
          </IonItem>
          <IonItem lines="none">
            <IonLabel position="fixed">Country</IonLabel>
            <IonText>{user.country}</IonText>
          </IonItem>

          <IonListHeader mode="ios">
            <IonLabel>Password</IonLabel>
            <IonButton onClick={() => present_editUserPassword()}>
              <IonIcon icon={createOutline}></IonIcon>
            </IonButton>
          </IonListHeader>
          <IonListHeader mode="ios">
            <IonLabel>Bank Account</IonLabel>
            <IonButton>
              <IonIcon icon={createOutline}></IonIcon>
            </IonButton>
          </IonListHeader>
        </IonGrid>
      </IonContent>
    </IonPage>
  )
}
