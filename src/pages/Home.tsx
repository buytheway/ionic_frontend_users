import {
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react"
import { useParams } from "react-router"
import ExploreContainer from "../components/ExploreContainer"
import "./Page.css"

const Home: React.FC = () => {
  // const { name } = useParams<{ name: string; }>();

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Go</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
        <ExploreContainer name="Go" />
      </IonContent>
    </IonPage>
  )
}

export default Home
