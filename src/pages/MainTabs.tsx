import {
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
} from "@ionic/react"
import {
  listOutline,
  locationOutline,
  personCircleOutline,
  receiptOutline,
  rocketOutline,
  storefrontOutline,
} from "ionicons/icons"
import React from "react"
import { Redirect, Route, Switch, useRouteMatch } from "react-router"
import Home from "./Home"
import { Locations } from "./Locations"
import { Markt } from "./Markt"
import { Activities } from "./Me"
import { WishLists } from "./WishLists"

interface MainTabsProps {}

const MainTabs: React.FC<MainTabsProps> = () => {
  const match = useRouteMatch()
  const currRootPath = match.url

  return (
    <IonTabs>
      <IonRouterOutlet>
        <Switch>
          <Route path={`${currRootPath}/home`} exact>
            <Home />
          </Route>
          <Route path={`${currRootPath}/markt`} exact>
            <Markt />
          </Route>
          <Route path={`${currRootPath}/locations`} exact>
            <Locations />
          </Route>
          <Route path={`${currRootPath}/wishlists`} exact>
            <WishLists />
          </Route>
          <Route path={`${currRootPath}/activities`} exact>
            <Activities />
          </Route>
        </Switch>
        <Redirect exact path={currRootPath} to={`${currRootPath}/home`} />
      </IonRouterOutlet>

      <IonTabBar slot="bottom">
        <IonTabButton tab="home" href={`${currRootPath}/home`}>
          <IonIcon ios={rocketOutline} md={rocketOutline} />
          <IonLabel>Go</IonLabel>
        </IonTabButton>
        <IonTabButton tab="Markt" href={`${currRootPath}/markt`}>
          <IonIcon ios={storefrontOutline} md={storefrontOutline} />
          <IonLabel>Markt</IonLabel>
        </IonTabButton>
        <IonTabButton tab="Locations" href={`${currRootPath}/locations`}>
          <IonIcon ios={locationOutline} md={locationOutline} />
          <IonLabel>Locations</IonLabel>
        </IonTabButton>
        <IonTabButton tab="Lists" href={`${currRootPath}/wishlists`}>
          <IonIcon ios={listOutline} md={listOutline} />
          <IonLabel>Lists</IonLabel>
        </IonTabButton>
        <IonTabButton tab="Activities" href={`${currRootPath}/activities`}>
          <IonIcon ios={receiptOutline} md={receiptOutline} />
          <IonLabel>Activities</IonLabel>
        </IonTabButton>
      </IonTabBar>
    </IonTabs>
  )
}

export default MainTabs
