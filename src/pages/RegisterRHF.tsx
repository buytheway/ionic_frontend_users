import {
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonMenuButton,
  IonPage,
  IonRow,
  IonText,
  IonTitle,
  IonToolbar
} from "@ionic/react"
import { useForm } from "react-hook-form"
import * as yup from "yup"
import { IonInputRHF } from "../utilComponents/IonInputRHF"
import { useYupValidationResolver } from "../utilHooks/useYupValidationResolver"
import styles from "./Register.module.scss"

interface Props {}

interface IFormInputs {
  username: string
  email: string
  password: string
  confirm_password: string
}

const initialValue = {
  username: "",
  email: "",
  password: "",
  confirm_password: "",
}

const schema = yup.object().shape({
  username: yup.string().min(5).required(),
  email: yup.string().email().required(),
  password: yup.string().min(3).required(),
  confirm_password: yup
    .string()
    .oneOf([yup.ref("password"), null], "Password must match")
    .required(),
})

export const RegisterRHF = (props: Props) => {
  const resolver = useYupValidationResolver(schema)

  const { control, register, handleSubmit, reset, formState } =
    useForm<IFormInputs>({
      mode: "onChange",
      resolver: resolver,
      defaultValues: initialValue,
    })
  return (
    <IonPage id={styles["registerPage"]}>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Register</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <IonRow className={styles.loginLogo}>
          <IonCol>
            <img src="assets/img/btw_logo.png" alt="buy the way logo" />
          </IonCol>
        </IonRow>
        <form noValidate>
          <IonList>
            <IonItem>
              <IonLabel position="stacked" color="primary">
                user name
              </IonLabel>
              <IonInputRHF
                control={control as any}
                name="username"
                type="text"
              />
            </IonItem>
            <IonItem>
              <IonLabel position="stacked" color="primary">
                Email
              </IonLabel>
              <IonInputRHF
                control={control as any}
                type="email"
                name="email"
                required
              />
            </IonItem>
            <IonItem>
              <IonLabel position="stacked" color="primary">
                Password
              </IonLabel>
              <IonInputRHF
                control={control as any}
                type="password"
                name="password"
              />
            </IonItem>
            <IonItem>
              <IonLabel position="stacked" color="primary">
                Confirm Password
              </IonLabel>
              <IonInputRHF
                control={control as any}
                type="password"
                name="confirm_password"
              />
            </IonItem>
          </IonList>
          <IonRow>
            <IonCol>
              <IonButton
                onClick={(e) => reset(initialValue)}
                color="light"
                expand="block"
              >
                Reset
              </IonButton>
            </IonCol>
            <IonCol>
              <IonButton onClick={(e) => {}} expand="block">
                Register
              </IonButton>
            </IonCol>
          </IonRow>
        </form>
        <IonRow>
          <IonCol>
            <IonText color="primary">
              Already have an account? Then click here to login
            </IonText>
          </IonCol>
        </IonRow>
        <IonRow>
          <IonCol>
            <IonButton expand="block">Login</IonButton>
          </IonCol>
        </IonRow>
      </IonContent>
    </IonPage>
  )
}
