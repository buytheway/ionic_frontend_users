import {
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonMenuButton,
  IonPage,
  IonRow,
  IonText,
  IonTitle,
  IonToolbar,
} from "@ionic/react"
import React, { useCallback, useState } from "react"
import useAuth from "../hooks/useAuth"
import "./Login.scss"
import {
  useForm,
  Controller,
  SubmitHandler,
  useController,
  UseControllerProps,
} from "react-hook-form"
import { IonInputRHF } from "../utilComponents/IonInputRHF"

import { DevTool } from "@hookform/devtools"
import { beakerSharp } from "ionicons/icons"

import * as yup from "yup"
import { useYupValidationResolver } from "../utilHooks/useYupValidationResolver"

interface LoginProps {}

interface IFormInputs {
  email: string
  password: string
}

let initialValues = {
  email: "",
  password: "",
}

const validationSchema = yup.object({
  email: yup.string().required("Required").email(),
  password: yup.string().required("Required").min(3),
})

const Login: React.FC<LoginProps> = () => {

  const { login } = useAuth()
  const resolver = useYupValidationResolver(validationSchema)

  const {
    register,
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<IFormInputs>({
    resolver,
    mode: "onChange",
    defaultValues: initialValues,
  })

  const loginHandler: SubmitHandler<IFormInputs> = async (data) => {
    console.log("data: ", data)
    await login(data.email, data.password)
  }

  /**
   *
   * @param _fieldName
   */
  const showError = (_fieldName: keyof IFormInputs) => {
    return (
      errors[_fieldName] && (
        <div
          style={{
            color: "red",
            padding: 5,
            paddingLeft: 12,
            fontSize: "smaller",
          }}
        >
          {_fieldName}:{" "}
          {errors[_fieldName]!.message || "This field is required"}
        </div>
      )
    )
  }

  return (
    <IonPage id="login-page">
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>

          <IonTitle>Login</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <div className="login-logo">
          <img src="assets/img/btw_logo.png" alt="buy the way logo" />
        </div>
        <form noValidate>
          <IonList>
            <IonItem>
              <IonLabel position="stacked" color="primary">
                Email
              </IonLabel>
              <IonInputRHF
                control={control as any}
                type="email"
                name="email"
                required
              />
            </IonItem>

            <IonItem>
              <IonLabel position="stacked" color="primary">
                Password
              </IonLabel>
              <IonInputRHF
                name="password"
                type="password"
                control={control as any}
              />
            </IonItem>
          </IonList>

          <IonRow>
            <IonCol>
              <IonButton expand="block" onClick={handleSubmit(loginHandler)}>
                Login
              </IonButton>
            </IonCol>

            <IonCol>
              <IonButton
                onClick={(e) => reset(initialValues)}
                expand="block"
                color="light"
              >
                Reset
              </IonButton>
            </IonCol>
          </IonRow>
        </form>

        <IonRow className="ion-padding-top">
          <IonCol>
            <IonText color="primary">
              Dont' have an account? Click here to signup
            </IonText>
          </IonCol>
        </IonRow>

        <IonRow>
          <IonCol>
            <IonButton routerLink="/signup" color="primary" expand="block">
              Signup
            </IonButton>
          </IonCol>
        </IonRow>

        <IonRow className="ion-padding-top">
          <IonCol>
            <IonText color="primary">
              Forget your password? Click here to change password
            </IonText>
          </IonCol>
        </IonRow>
      </IonContent>
    </IonPage>
  )
}

export default Login
