import {
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonHeader,
  IonLabel,
  IonMenuButton,
  IonPage,
  IonRow,
  IonSegment,
  IonSegmentButton,
  IonText,
  IonTitle,
  IonToolbar,
  NavContext,
} from "@ionic/react"
import React, { useContext, useState } from "react"
import { act } from "react-dom/test-utils"
import { Redirect, useHistory, useParams } from "react-router"
import classes from "./Login_and_Register.module.scss"
import { capitalize } from "lodash"
import { LoginComp } from "../components/LoginComp"
import { RegisterComp } from "../components/RegisterComp"
import useAuth from "../hooks/useAuth"

interface Props {}

export const Login_and_Register: React.FC = (props: Props) => {
  let { action } = useParams<{ action: string }>()
  const [authAction, setAuthAction] = useState(action)
  const { navigate } = useContext(NavContext)
  const auth = useAuth()
  console.log(`Login_and_Register`, "hello")

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>{capitalize(action)}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding" id={classes["AuthPage"]}>
        <IonRow className={classes.authLogo}>
          <IonCol>
            <img src="assets/img/btw_logo.png" alt="buy the way logo" />
          </IonCol>
        </IonRow>
        <IonSegment
          value={authAction}
          onIonChange={(e) => {
            console.log(e.detail.value)
            setAuthAction(e.detail.value!)
            e.preventDefault()
            navigate("/auth/" + e.detail.value, "none", "replace")
          }}
        >
          <IonSegmentButton value="login">
            <IonLabel>Login</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="register">
            <IonLabel>Register</IonLabel>
          </IonSegmentButton>
        </IonSegment>
        <IonRow>
          <IonCol>
            {authAction === "login" ? <LoginComp /> : <RegisterComp />}
          </IonCol>
        </IonRow>
      </IonContent>
    </IonPage>
  )
}
