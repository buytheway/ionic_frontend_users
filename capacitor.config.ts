import { CapacitorConfig } from "@capacitor/cli"

const config: CapacitorConfig = {
  appId: "io.ionic.starter",
  appName: "btw_v3_client",
  webDir: "build",
  bundledWebRuntime: false,
  server: {
    url: "http://192.168.0.164:8100",
    cleartext: true,
  },
}

export default config
